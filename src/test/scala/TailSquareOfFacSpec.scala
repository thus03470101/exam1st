import org.scalatest.FunSuite
import Questions._
/** 15%
  * 請用Tail Recursive寫出平方階乘函數squareOfFac(n)=[n*n]*[(n-1)*(n-1)]*...*(2*2)*(1*1)
  * ex:
  * squareOfFac(3)=36
  * squareOfFac(5)=14400
  **/
class TailSquareOfFacSpec extends FunSuite{
  test("tailSquareOfFac(5,1)=14400") {
    assert(tailSquareOfFac(5,1)==14400)

  }
  test("tailSquareOfFac(3,1)=36") {
    assert(tailSquareOfFac(3,1)==36)

  }
  test("tailSquareOfFac(1,1)=1") {
    assert(tailSquareOfFac(1,1)==1)

  }
  test("tailSquareOfFac(10000000,1)") {
    assert(tailSquareOfFac(10000000,1)==0)

  }


}
